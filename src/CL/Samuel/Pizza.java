/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CL.Samuel;

/**
 *
 * @author Samuel Vilches
 */
public class Pizza {
    // Paso 1 Crear atributos: 
    private String nombre;
    private String tamanio;
    private String masa;
    
    //Paso2: Construir los métodos: (Customer)
    public void preparar (){
        System.out.println("Estamos preparando su pizza");
    }
    public void calentar (){
        System.out.println("Estamos horneando su pizza");
    }
    //Paso 4: Crear constructor con y sin parámetros

    public Pizza() {
    }

    public Pizza(String nombre, String tamanio, String masa) {
        this.nombre = nombre;
        this.tamanio = tamanio;
        this.masa = masa;
    }
    
    // Paso 5: Creat métodos setter y getter

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    public String getMasa() {
        return masa;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }
    
    //Paso 6: Crear método toString

    @Override
    public String toString() {
        return "Pizza{" + "nombre=" + nombre + ", tamanio=" + tamanio + ", masa=" + masa + '}';
    }
    
    
}
