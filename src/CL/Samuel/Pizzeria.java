/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CL.Samuel;

/**
 *
 * @author CETECOM
 */
public class Pizzeria {
    public static void main(String[] args) {
        //Paso 3: Instanciar 2 objetos
        Pizza pizza1 = new Pizza();
        pizza1.getNombre();
        System.out.println(pizza1.getNombre());
        pizza1.setNombre("Española");
        System.out.println("Nombre pizza 1: " + pizza1.getNombre());
        
        Pizza pizza2 = new Pizza("Pepperoni","Familiar","Normal");
        pizza2.setNombre("Vegetariana");
        System.out.println(pizza2.toString());
        
        //Solo el nombre, solo el tamaño y los 3 atributos de la pizza
        
        Pizza pizza3 = new Pizza("Barbacue","Extra Grande","Normal");
        pizza3.preparar();
        System.out.println(pizza3.getNombre());
        System.out.println(pizza3.getTamanio());
        System.out.println(pizza3.toString());
        pizza3.calentar();
        
    }
}
